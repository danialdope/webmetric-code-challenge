import * as React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Container from '@mui/material/Container'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'

import TabPanel from './components/TabPanel'
import Todos from './components/Todos'
import AddNewTaskDialog from './components/AddNewTaskDialog'

import { TFilter } from './types'

import { RootState } from './store/store'
import { setFilter } from './store/slices/todos.slice'



function App() {
  const dispatch = useDispatch();
  const { todos, filter } = useSelector((state: RootState) => state.todos)
  const [activeTab, setActiveTab] = React.useState<TFilter>(filter || 'all');
  const [addNewTodoDialog, setAddNewTodoDialog] = React.useState(false);

  const handleFilterChange = (_event: React.SyntheticEvent, newValue: TFilter) => {
    dispatch(setFilter(newValue))
    setActiveTab(newValue);
  };

  // Filters an array of todos based on a given filter value 
  // and stores the resulting array in the 'filteredTodos' constant.
  const filteredTodos = todos.filter((todo) => {
    if (filter === 'completed') {
      return todo.completed;
    } else if (filter === 'ongoing') {
      return !todo.completed;
    } else {
      return true;
    }
  });

  return (
    <>
      <Container maxWidth='md'>
        <Box borderBottom={1} borderColor={'divider'}>
          <Tabs
            value={activeTab}
            onChange={handleFilterChange}
            centered
            variant='fullWidth'
          >
            <Tab value='all' label='All Tasks' />
            <Tab value='ongoing' label='Ongoing' />
            <Tab value='completed' label='Completed' />
          </Tabs>
        </Box>

        <Box textAlign={'right'} paddingY={2}>
          <Button variant='outlined' startIcon={<AddIcon />} onClick={() => setAddNewTodoDialog(true)} >
            New Task
          </Button>
        </Box>

        {['all', 'ongoing', 'completed'].map((value) => (
          <TabPanel key={value} value={value} activePanel={activeTab}>
            <Todos data={filteredTodos} filter={filter} />
          </TabPanel>
        ))}

        <AddNewTaskDialog open={addNewTodoDialog} onClose={() => setAddNewTodoDialog(false)} />
      </Container>
    </>
  )
}

export default App
