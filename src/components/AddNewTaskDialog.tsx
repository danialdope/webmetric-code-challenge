import { FC } from 'react'
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { addTodo } from '../store/slices/todos.slice'



import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box'

interface AddNewTaskDialogProps {
  open: boolean,
  onClose?: () => void
}

type FormValues = {
  title: string,
  description?: string
}

const AddNewTaskDialog: FC<AddNewTaskDialogProps> = ({ onClose, open }) => {
  const dispatch = useDispatch()
  const methods = useForm<FormValues>();

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    dispatch(addTodo({
      id: Math.random().toString(36).substring(2, 9),
      title: data.title,
      description: data.description,
      completed: false
    }))
    methods.reset()
    onCloseHandler()
  };

  const onCloseHandler = () => {
    methods.reset()
    onClose ? onClose() : ''
  }

  return (
    <Dialog open={open} onClose={onCloseHandler}>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(onSubmit)}>
          <DialogTitle>Add New Task</DialogTitle>
          <DialogContent>
            <Box pb={2}>
              <DialogContentText>
                Enter the details for your new task below
              </DialogContentText>
            </Box>
            <TextField
              autoFocus
              margin='dense'
              placeholder='Enter title'
              id='name'
              label='Title'
              type='text'
              fullWidth
              InputLabelProps={{
                shrink: true,
              }}
              variant='outlined'
              {...methods.register('title',
                {
                  required: true
                }
              )}
            />
            <TextField
              autoFocus
              margin='dense'
              id='description'
              label='Description'
              placeholder='Enter description'
              type='text'
              fullWidth
              multiline
              rows={2}
              InputLabelProps={{
                shrink: true,
              }}
              variant='outlined'
              {...methods.register('description')}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={onCloseHandler}>Cancel</Button>
            <Button type='submit'>Subscribe</Button>
          </DialogActions>
        </form>
      </FormProvider>
    </Dialog>
  )
}

export default AddNewTaskDialog;