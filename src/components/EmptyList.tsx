import { FC } from 'react'
import TypoGraphy from '@mui/material/Typography'
import Box from '@mui/material/Box'
import { TFilter } from '../types'

interface EmptyListProps {
  filter: TFilter
}
const filterText = {
  all: 'Your todo list is empty. Add a new task to get started!',
  completed: 'You have no completed tasks yet.',
  ongoing: 'You have no ongoing tasks at the moment.',
};

const EmptyList: FC<EmptyListProps> = ({filter}) => {
  const description = filterText[filter];
  return (
    <Box textAlign={'center'}>
      <TypoGraphy>
        {description}
      </TypoGraphy>
    </Box>
  )
}

export default EmptyList;