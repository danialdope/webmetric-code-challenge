import * as React from 'react';
import Box from '@mui/material/Box';

interface TabPanelProps {
  children?: React.ReactNode;
  activePanel: number | string;
  value: number | string;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, activePanel, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== activePanel}
      id={`simple-tabpanel-${activePanel}`}
      aria-labelledby={`simple-tab-${activePanel}`}
      {...other}
    >
      {value === activePanel && (
        <Box sx={{ pb: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
}

export default TabPanel