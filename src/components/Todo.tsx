import { FC } from 'react'
import { useDispatch } from 'react-redux'
import { toggleTodo } from '../store/slices/todos.slice'

import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Checkbox from '@mui/material/Checkbox'
import FormControlLabel from '@mui/material/FormControlLabel'
import Stack from '@mui/material/Stack'
import { green } from '@mui/material/colors'

import { TTodo } from '../types'

interface TodoProps extends TTodo {

}

const Todo: FC<TodoProps> = (props) => {
  const { id, title, description, completed } = props

  const dispatch = useDispatch();

  const handleChange = () => {
    dispatch(toggleTodo(id))
  };

  return (
    <Card variant='outlined'>
      <CardContent>
        <Stack direction='row' justifyContent='space-between'>
          <Typography variant='h5' component='p'>
            {title}
          </Typography>
          <FormControlLabel control={
            <Checkbox 
              checked={completed}
              color={completed ? 'success' : 'primary'}
              onChange={handleChange}
              inputProps={{ 'aria-label': 'controlled' }}
            />
          }
            label={
              <span style={{color: completed ? green[700] : ''}}>Completed</span>
            }
          />
        </Stack>
        <Typography variant='body1' component='span'>
          {description}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default Todo;