import { FC } from 'react'
import Box from '@mui/material/Box'

import EmptyList from './EmptyList'
import Todo from './Todo'

import { TTodo, TFilter } from '../types'

interface TodosProps {
  data: TTodo[],
  filter: TFilter
}

const Todos: FC<TodosProps> = ({ data, filter }) => {
  return (
    <Box>
      {data.length < 1 && (
        <EmptyList filter={filter} />
      )}
      {data.map((value) => (
        <Box sx={{pb: 4}} key={value.id}>
          <Todo {...value} />
        </Box>
      ))}
    </Box>
  )
}

export default Todos;