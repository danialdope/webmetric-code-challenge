import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TTodo, TTodos } from '../../types';

const initialState: TTodos = {
  todos: [
    {
      id: 'abc',
      title: 'Meeting About Previous Task',
      description: 'Client Mobile App',
      completed: false
    },
    {
      id: 'abcd',
      title: 'Meeting With Client',
      description: 'Redline Admin Panel',
      completed: true
    }
  ],
  filter: 'all',
};

const todoSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<TTodo>) => {
      state.todos.push(action.payload);
    },
    toggleTodo: (state, action: PayloadAction<string>) => {
      const todo = state.todos.find((todo) => todo.id === action.payload);
      if (todo) {
        todo.completed = !todo.completed;
      }
    },
    setFilter: (state, action: PayloadAction<TTodos['filter']>) => {
      state.filter = action.payload;
    },
  },
});

export const { addTodo, toggleTodo, setFilter } = todoSlice.actions
export default todoSlice.reducer