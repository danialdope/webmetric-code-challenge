export type TTodo = {
  id: string;
  title: string;
  description?: string;
  completed: boolean;
};

export type TFilter = 'all' | 'completed' | 'ongoing'

export type TTodos = {
  todos: TTodo[];
  filter: TFilter
}